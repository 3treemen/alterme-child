<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

function custom_registration_form_redirect( $redirect ) {
    $redirect = site_url( '/mijn-account/', is_ssl() ? 'https' : 'http' );
    return $redirect;
}
add_filter( 'learndash-registration-form-redirect', 'custom_registration_form_redirect' );

function custom_login_form_args( $login_form_args ) {
    $login_form_args['redirect'] = site_url( '/mijn-account/', is_ssl() ? 'https' : 'http' );
    return $login_form_args;
}
add_filter( 'learndash-login-form-args', 'custom_login_form_args' );

function redirect_my_account( $registration_redirect ) {
    $redirect = site_url( '/mijn-account/', is_ssl() ? 'https' : 'http' );
    return redirect;
}
add_filter( 'registration_redirect', 'redirect_my_account' );


function wpse_lost_password_redirect() {
    wp_redirect( home_url() ); 
    exit;
}
add_action('after_password_reset', 'wpse_lost_password_redirect');


/* mailtesting, disable when going live */
function mailtrap($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.mailtrap.io';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 2525;
    $phpmailer->Username = 'a06cc5a64f96f2';
    $phpmailer->Password = '34b2f8b2bcd77a';
}
add_action('phpmailer_init', 'mailtrap');


##################################
//remove messy profile section items
##################################
if( is_admin() ){
    remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
    add_action( 'personal_options', 'prefix_hide_personal_options' );
}
 
function prefix_hide_personal_options() {
  ?>
    <script type="text/javascript">
        jQuery( document ).ready(function( $ ){
            $( '#your-profile .form-table:first, #your-profile h3:first, .yoast, .user-description-wrap, .user-profile-picture, h2, .user-pinterest-wrap, .user-myspace-wrap, .user-soundcloud-wrap, .user-tumblr-wrap, .user-wikipedia-wrap' ).remove();
            $( '.user-url-wrap').remove();
        } );
    </script>
  <?php
}

/**
 * disable application password except voor admin
 */
function my_prefix_customize_app_password_availability(
    $available,
    $user
) {
    if ( ! user_can( $user, 'manage_options' ) ) {
        $available = false;
    }
 
    return $available;
}
 
add_filter(
    'wp_is_application_passwords_available_for_user',
    'my_prefix_customize_app_password_availability',
    10,
    2
);