Documentation of the prototype setup proces

Base plugin is LeardDash: $149 first year

Plugin user-menus is used to add a contextual menu 'mijn account', only visible for logged-in users

function custom_login_form_args() is then setup to redirect a user to 'mijn-account' page after login.

The mijn-account page is a standard page with a learndash-block 'profile'.

Local install of Mailhog for email testing: https://github.com/mailhog/MailHog